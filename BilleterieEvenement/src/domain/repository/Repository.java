package domain.repository;

import domain.aggregates.ReservationEvent;

public interface Repository {

	public ReservationEvent findById(int id);
	
	public void save(ReservationEvent reservation);
	
}
