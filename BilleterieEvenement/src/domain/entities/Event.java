package domain.entities;

import java.time.LocalDate;

import domain.vo.Duration;
import domain.vo.Place;
import exceptions.EventFullException;

public class Event {

	private int id;
	private int reservations = 0;
	private LocalDate date;
	private String name;
	private Duration duration;
	private Place place;
	private float price;
	private float income = 0;
	
	/**
	 * @param reservations
	 * @param localDate
	 * @param name
	 * @param duration
	 */
	public Event(int id, LocalDate localDate, String name, Place place, Duration duration, float price) {
		this.id = id;
		this.date = localDate;
		this.name = name;
		this.duration = duration;
		this.place = place;
		this.price = price;
	}

	
	
	/**
	 * @return the reservations
	 */
	public int getReservations() {
		return reservations;
	}
	
	/**
	 * @return the date
	 */
	public LocalDate getDate() {
		return date;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}
	
	/**
	 * @return the place
	 */
	public Place getPlace() {
		return place;
	}


	public Integer getId() {
		return id;
	}
	
	public boolean isFull() {
		return this.reservations == this.place.getCapacity();
	}
	
	public void takeReservation(int placesToTake) throws Exception,EventFullException {
		if(placesToTake<0) {
			throw new Exception("Can't book negative amount");
		}
		if(reservations + placesToTake > place.getCapacity()) {
			int placesLeft = place.getCapacity()-reservations;
			throw new EventFullException("Could not take reservation for "+placesToTake +" places. Event has only "+ placesLeft +" places left.");
		}
		reservations += placesToTake;
		income += placesToTake*price;
	}
	
	public void freeReservation(int placesToFree) throws Exception {
		if(placesToFree<0) {
			throw new Exception("Can't free negative amount");
		}
		reservations = Math.max(reservations - placesToFree, 0);
		income -= placesToFree*price;
	}

	private String readableIncome() {
		
		return String.format("%,d", (int)income) + "."+(int)((income % 1) * 100);
	}

	@Override
	public String toString() {
		return name+"\n\treservations : " + reservations + "\n\tdate : " + date+ "\n\tduration : " + duration + "\n\tplace : " + place+"\n\tprice : $"+price+"\n\tincome ≈ $"+ readableIncome() +"\n";
	}
	
	
}
