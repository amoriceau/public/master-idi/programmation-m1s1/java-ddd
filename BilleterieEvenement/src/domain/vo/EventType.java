package domain.vo;

public class EventType {

	private String name;

	/**
	 * @param name
	 */
	public EventType(String name) {
		super();
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	
}
