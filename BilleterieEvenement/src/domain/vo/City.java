package domain.vo;

public class City {

	private String name;
	private int zip_code;

	/**
	 * @param name
	 */
	public City(String name, int zip_code) {
		this.name = name;
		this.zip_code = zip_code;
	}

	public String getName() {
		return name;
	}

	/**
	 * @return the zip_code
	 */
	public int getZipCode() {
		return zip_code;
	}
	
	@Override
	public String toString() {
		return name + ", " + zip_code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + zip_code;
		return result;
	}


	public boolean equals(City obj) {
		return this.name == obj.name && this.zip_code == obj.zip_code;
	}
	
	
	
}
