package domain.vo;

public class Address {

	private String street;
	private int number;
	private City city;
	
	/**
	 * @param street
	 * @param number
	 * @param city
	 * @param zip_code
	 */
	public Address(String street, int number, City city) {
		this.street = street;
		this.number = number;
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public int getNumber() {
		return number;
	}

	public City getCity() {
		return city;
	}

	@Override
	public String toString() {
		return "address : " + street + ", " + number + ", " + city;
	}

	
}
