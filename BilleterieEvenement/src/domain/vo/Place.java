package domain.vo;

public class Place {

	private String name;
	private Address address;
	private int capacity;
	
	/**
	 * @param name
	 * @param address
	 * @param capacity
	 */
	public Place(String name, Address address, int capacity) {
		this.name = name;
		this.address = address;
		this.capacity = capacity;
	}

	public String getName() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public int getCapacity() {
		return capacity;
	}

	@Override
	public String toString() {
		return name + "\n\t" + address + "\n\tcapacity: " + capacity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + capacity;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	public boolean equals(Place obj) {

		return this.name == obj.name && this.address.getCity().equals(obj.address.getCity());
	}

	
	
	
	  
}
