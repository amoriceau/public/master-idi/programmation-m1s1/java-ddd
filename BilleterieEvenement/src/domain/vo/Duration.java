package domain.vo;

public class Duration {

	private int days;
	private int hours;
	private int minutes;
	/**
	 * @param days
	 * @param hours
	 * @param minutes
	 */
	public Duration(int days, int hours, int minutes) {
		this.days = days;
		this.hours = hours;
		this.minutes = minutes;
	}
	
	public Duration(int hours, int minutes) {
		this(0, hours, minutes);
	}
	
	/**
	 * @return the days
	 */
	public int getDays() {
		return days;
	}

	/**
	 * @return the hours
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * @return the minutes
	 */
	public int getMinutes() {
		return minutes;
	}

	@Override
	public String toString() {
		String duration = "";
		if(days != 0) {
			duration+=days+"D ";
		}
		if(hours != 0) {
			duration+=hours+"H ";
		}
		if(minutes != 0) {
			duration+=minutes+"M ";
		}
		return duration;
	}
	
	
	
	
}
