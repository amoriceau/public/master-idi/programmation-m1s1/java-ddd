package application;

import java.time.LocalDate;
import java.util.List;

import domain.aggregates.ReservationEvent;
import domain.entities.Event;
import domain.repository.Repository;
import domain.vo.Duration;
import domain.vo.EventType;
import domain.vo.Place;
import exceptions.EventFullException;
import exceptions.EventNotFoundException;
import infrastructure.ReservationRepository;

public class BilleterieApplication {

	private final Repository repo = new ReservationRepository();
	private static int cpt = 0;
	
	public int createAggregate(EventType type) {
		ReservationEvent r = new ReservationEvent(++cpt, type);
		repo.save(r);
		return cpt;
	}
	
	public List<Integer> getListEvents(int aggid){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEvents();
	}
	
	public List<Integer> getListEvents(int aggid, LocalDate date){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEvents(date);
	}
	
	public List<Integer> getListEvents(int aggid, LocalDate from, LocalDate to){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEvents(from, to);
	}
	
	public List<Integer> getListEvents(int aggid, Place place, LocalDate from, LocalDate to){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEvents(place, from, to);
	}
	
	public List<Integer> getListEvents(int aggid, Place place){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEvents(place);
	}
	
	public List<Integer> getListEventsFrom(int aggid, LocalDate date){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEventsFrom(date);
	}
	
	public List<Integer> getListEventsBefore(int aggid, LocalDate date){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEventsBefore(date);
	}
	
	public String getEventAsString(int aggid, int eid){
		ReservationEvent r = repo.findById(aggid);
		return r.getEventAsString(eid);
	}
	
	public int addEvent(int aggid, LocalDate localDate, String name, Place place, Duration duration, float price){
		ReservationEvent r = repo.findById(aggid);
		return r.addEvent(localDate, name, place, duration, price);
	}
	
	public boolean removeEvent(int aggid, Event event){
		ReservationEvent r = repo.findById(aggid);
		return r.removeEvent(event);
	}
	
	public boolean takeReservation(int aggid, int eid, int placesToTake) throws EventNotFoundException, EventFullException {
		ReservationEvent r = repo.findById(aggid);
		return r.takeReservation(eid, placesToTake);
	}
	public boolean freeReservation(int aggid, int eid, int placesToFree) throws Exception {
		ReservationEvent r = repo.findById(aggid);
		return r.freeReservation(eid, placesToFree);
	}

	public boolean isEventFull(int aggid, int eid) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.isEventFull(eid);
	}
	
	public int getNumberOfVisitorsInPlace(int aggid, Place place) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.getNumberOfVisitorsInPlace(place); 
	}
	
	public int getNumberOfVisitorsInPlace(int aggid, Place place, LocalDate from, LocalDate to) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.getNumberOfVisitorsInPlace(place, from, to); 
	}
	
	
	public int getEventMaxCapacity(int aggid, int eid) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.getEventMaxCapacity(eid);
	}
	
	public int getEventReservations(int aggid, int eid) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.getEventReservations(eid);
	}


}
