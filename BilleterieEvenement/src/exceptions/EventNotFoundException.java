package exceptions;

public class EventNotFoundException extends Exception {

	public EventNotFoundException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6223341186239846773L;

}
