package exceptions;

public class EventFullException extends Exception {

	public EventFullException(String string) {
		super(string);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 457532919276325007L;

}
