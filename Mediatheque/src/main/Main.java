package main;

import application.MediathequeApp;
import domain.exceptions.ImpossibleReservationException;
import domain.exceptions.InvalidMediaException;
import domain.exceptions.MediaNotFoundException;
import domain.vo.Book;
import domain.vo.MediaType;
import domain.vo.Movie;

public class Main {
	public static void main(String[] args) throws InvalidMediaException, MediaNotFoundException, ImpossibleReservationException {
		MediathequeApp app = new MediathequeApp();
		
		
		
		
		// VO
		Book b1 = new Book("L'étranger", "Albert Camus");
		Book b2 = new Book("La relativité générale", "Albert Einstein");
		
		Movie m1 = new Movie("Pulp Fiction", "Q. Tarantino");
		
		// AGGREGATES
		int bookAggId = app.createAgg(MediaType.BOOK);
		int movieAggId = app.createAgg(MediaType.MOVIE);
		
		
		// ENTITIES VIA AGGREGATE
		int bookEntity = app.addMedia(bookAggId, b2);
		
		// Avoid to crash the app for this particular case
		int movieEntity = app.addMedia(movieAggId, m1, 25);	
		try {
			int bookEntity2 = app.addMedia(bookAggId, m1);	
		}catch(InvalidMediaException e) {
			System.out.println(e.getMessage());	
			
		}
		
		for (Integer mid : app.getMediasList(bookAggId)) {
			System.out.println(app.getMediaAsString(bookAggId, mid));
		}
		
		for (Integer mid : app.getMediasList(movieAggId)) {
			System.out.println(app.getMediaAsString(movieAggId, mid));
		}
		
		int bookRes1 = app.addReservation(bookAggId, bookEntity, 30);
		
		for (Integer rid : app.getReservationsList(bookAggId)) {
			System.out.println(app.getReservationAsString(bookAggId, rid));
		}
		for (Integer mid : app.getMediasList(bookAggId)) {
			System.out.println(app.getMediaAsString(bookAggId, mid));
		}
		
		try {
			app.addReservation(bookAggId, bookEntity, 30);	
		}catch(ImpossibleReservationException e) {
			System.out.println(e.getMessage());
		}
		
		app.cancelReservation(bookAggId, bookRes1);
		
		for (Integer rid : app.getReservationsList(bookAggId)) {
			System.out.println(app.getReservationAsString(bookAggId, rid));
		}
		for (Integer mid : app.getMediasList(bookAggId)) {
			System.out.println(app.getMediaAsString(bookAggId, mid));
		}
		
	}
}
