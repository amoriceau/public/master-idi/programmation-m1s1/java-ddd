package infra;

import java.util.HashMap;

import domain.aggregate.MediaReservation;
import domain.repository.Repository;

public class MediaRepository implements Repository {

	
	private HashMap<Integer, MediaReservation> storage = new HashMap<>(); 
	@Override
	public void save(MediaReservation reservation) {
		storage.put(reservation.getId(), reservation);
	}

	@Override
	public MediaReservation findById(int id) {
		return storage.get(id);
	}

}
