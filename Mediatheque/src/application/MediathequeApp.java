package application;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import domain.aggregate.MediaReservation;
import domain.exceptions.ImpossibleReservationException;
import domain.exceptions.InvalidMediaException;
import domain.exceptions.MediaNotFoundException;
import domain.repository.Repository;
import domain.vo.Media;
import domain.vo.MediaType;
import infra.MediaRepository;

public class MediathequeApp {

	Repository repo = new MediaRepository();
	private static int cpt = 0;
	
	public int createAgg(MediaType media) {
		MediaReservation agg = new MediaReservation(++cpt,media);
		repo.save(agg);
		return agg.getId();
	}
	
	public int addMedia(int id, Media media) throws InvalidMediaException {
		MediaReservation agg = repo.findById(id);
		return agg.addMedia(media);
	}
	
	public int addMedia(int id, Media media, int copies) throws InvalidMediaException {
		MediaReservation agg = repo.findById(id);
		return agg.addMedia(media, copies);
	}
	
	public int addReservation(int id, int mid, int duration) throws InvalidMediaException, MediaNotFoundException, ImpossibleReservationException {
		MediaReservation agg = repo.findById(id);
		return agg.addReservation(mid, duration);
	}
	
	public boolean cancelReservation(int id, int rid) throws InvalidMediaException, MediaNotFoundException, ImpossibleReservationException {
		MediaReservation agg = repo.findById(id);
		return agg.cancelReservation(rid);
	}

	public String getReservationAsString(int id, int resId) {
		MediaReservation agg = repo.findById(id);
		return agg.getReservationAsString(resId);
	}
	
	public String getMediaAsString(int id, int mid) {
		MediaReservation agg = repo.findById(id);
		return agg.getMediaAsString(mid);
	}
	
	
	public List<Integer> getMediasList(int id){
		MediaReservation agg = repo.findById(id);
		return agg.getMediasList();
	}
	
	public List<Integer> getReservationsList(int id){
		MediaReservation agg = repo.findById(id);
		return agg.getReservationsList();
	}
	
}
