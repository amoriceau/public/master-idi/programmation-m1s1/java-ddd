package domain.vo;

public class Book extends Media{

	public Book(String name, String author) {
		super(name, author);
	}

	@Override
	public String mediaType() {
		return MediaType.BOOK.toString();
	}

	
}
