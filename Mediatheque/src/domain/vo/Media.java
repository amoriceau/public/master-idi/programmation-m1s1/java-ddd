package domain.vo;

public abstract class Media {
	private String name;
	private String author;
	
	/**
	 * @param name
	 * @param author
	 */
	public Media(String name, String author) {
		this.name = name;
		this.author = author;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	@Override
	public String toString() {
		return "Media [name=" + name + ", author=" + author + "]";
	}
	
	public abstract String mediaType();
	
	
	
}
