package domain.vo;

public class Movie extends Media{

	public Movie(String name, String author) {
		super(name, author);
	}

	@Override
	public String mediaType() {
		return MediaType.MOVIE.toString();
	}

	
}
