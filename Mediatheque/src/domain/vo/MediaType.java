package domain.vo;

public enum MediaType {
	BOOK {
		@Override
		public String toString() {
			return "BOOK";
		}
	},
	MOVIE {
		@Override
		public String toString() {
			return "MOVIE";
		}
	},
}
