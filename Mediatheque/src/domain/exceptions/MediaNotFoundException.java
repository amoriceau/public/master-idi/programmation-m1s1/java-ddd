package domain.exceptions;

public class MediaNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4609557813342584396L;
	
	public MediaNotFoundException(String msg) {
		super(msg);
	}

}
