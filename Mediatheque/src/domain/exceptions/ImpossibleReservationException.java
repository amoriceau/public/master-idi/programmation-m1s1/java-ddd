package domain.exceptions;

public class ImpossibleReservationException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8146319895461277128L;

	public ImpossibleReservationException() {
		super("Impossible to book this media, it has no available copies.");
	}
}
