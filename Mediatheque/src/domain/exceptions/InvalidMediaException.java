package domain.exceptions;

import domain.vo.MediaType;

public class InvalidMediaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6920580756505711029L;

	public InvalidMediaException(MediaType media, String received) {
		super("Invalid media type, expecting "+media.toString() + " but received " + received);
	}
}
