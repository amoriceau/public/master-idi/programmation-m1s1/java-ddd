package domain.entity;

import domain.vo.Media;

public class MediaEntity {

	private int id;
	private Media media;
	private int copies = 0;
	private int availableCopies = 0;
	
	public MediaEntity(int id, Media media, int copies) {
		this(id, media);
		this.copies +=copies - 1;
		this.availableCopies +=copies - 1;
	}
	
	public MediaEntity(int id, Media media) {
		this.id = id;
		this.media = media;
		this.copies = 1;
		this.availableCopies = 1;
	}

	/**
	 * @return the availableCopies
	 */
	public int getAvailableCopies() {
		return availableCopies;
	}
	
	/**
	 * @return the copies
	 */
	public int getCopies() {
		return copies;
	}

	/**
	 * @return the media
	 */
	public Media getMedia() {
		return media;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public void registerMedia() {
		this.copies++;
		this.availableCopies++;
	}
	
	public void registerMedia(int nb) {
		this.copies+=nb;
		this.availableCopies+=nb;
	}

	public boolean takeReservation() {
		if(availableCopies == 0) {
			return false;
		}
		availableCopies--;
		return true;
	}

	public void cancelReservation() {
		availableCopies++;
	}

	@Override
	public String toString() {
		return "MediaEntity [id=" + id + ", media=" + media + ", copies=" + copies + ", availableCopies="
				+ availableCopies + "]";
	}
	
	
}
