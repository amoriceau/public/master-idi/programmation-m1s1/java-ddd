package domain.repository;

import domain.aggregate.MediaReservation;

public interface Repository {

	public void save(MediaReservation reservation);
	
	public MediaReservation findById(int id);
}
