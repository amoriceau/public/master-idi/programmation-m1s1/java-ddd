package domain.aggregate;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import domain.entity.MediaEntity;
import domain.entity.Reservation;
import domain.exceptions.ImpossibleReservationException;
import domain.exceptions.InvalidMediaException;
import domain.exceptions.MediaNotFoundException;
import domain.vo.Media;
import domain.vo.MediaType;

public class MediaReservation {

	
	
	private MediaType mediaType;
	private List<Reservation> reservations = new LinkedList<>();
	private HashMap<Integer,MediaEntity> medias = new HashMap<>();
	private int id;
	private static int mediaCpt = 0;
	private static int resaCpt = 0;

	public MediaReservation(int id, MediaType mediaType) {
		this.id = id;
		this.mediaType = mediaType;
	}

	public Integer getId() {
		return id;
	}

	public String getReservationAsString(int resId) {
		return reservations.get(resId).toString();
	}

	public String getMediaAsString(int mid) {
		return medias.get(mid).toString();
	}
	
	public List<Integer> getMediasList(){
		List<Integer> tmp = new LinkedList<>();
		for (Entry<Integer, MediaEntity> entry : medias.entrySet()) {
			tmp.add(entry.getKey());
			
		}
		return tmp;
	}
	
	public List<Integer> getReservationsList(){
		List<Integer> tmp = new LinkedList<>();
		for (Reservation resa : reservations) {
			tmp.add(resa.getId());
		}
		return tmp;
	}

	public int addMedia(Media media) {
		for (Entry<Integer, MediaEntity> mediaEntity : medias.entrySet()) {
			if(mediaEntity.getValue().getMedia().equals(media)) {
				mediaEntity.getValue().registerMedia();
				return mediaEntity.getValue().getId();
			}
		}
		MediaEntity mediaEntity = new MediaEntity(++mediaCpt , media);
		medias.put(mediaEntity.getId(), mediaEntity);
		return mediaEntity.getId();
	}
	
	public int addMedia(Media media, int nb) throws InvalidMediaException {
		if(media.mediaType() != mediaType.toString()) {
			throw new InvalidMediaException(mediaType, media.mediaType());
		}
		for (Entry<Integer, MediaEntity> mediaEntity : medias.entrySet()) {
			if(mediaEntity.getValue().getMedia().equals(media)) {
				mediaEntity.getValue().registerMedia(nb);
				return mediaEntity.getValue().getId();
			}
			
		}
		MediaEntity mediaEntity = new MediaEntity(++mediaCpt , media, nb);
		medias.put(mediaEntity.getId(), mediaEntity);
		return mediaEntity.getId();
	}
	
	public int addReservation(MediaEntity media, int duration) throws MediaNotFoundException, ImpossibleReservationException {
		MediaEntity mediaE = null;
		for (Entry<Integer, MediaEntity> mediaEntity : medias.entrySet()) {
			if(mediaEntity.getKey() == media.getId()) {
				mediaE = mediaEntity.getValue();
			}
			
		}
		if(mediaE == null) {
			throw new MediaNotFoundException("Media not found, reservation could not have been taken");
		}
		if(!mediaE.takeReservation()) {
			throw new ImpossibleReservationException();
		}
		
		LocalDate today = LocalDate.now();
		Reservation resa = new Reservation(++resaCpt, mediaE, today, today.plusDays(duration));
		reservations.add(resa);
		return resa.getId();

	}
	
	public boolean cancelReservation(int reservationId) {
		return reservations.get(reservationId).cancelReservation();
		
	}

	/**
	 * @return the mediaType
	 */
	public MediaType getMediaType() {
		return mediaType;
	}


}
