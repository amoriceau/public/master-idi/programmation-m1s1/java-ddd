package domain.aggregates;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import domain.entities.Event;
import domain.vo.Duration;
import domain.vo.EventType;
import domain.vo.Place;
import exceptions.EventFullException;
import exceptions.EventNotFoundException;

public class ReservationEvent {
	private static int cpt = 0;
	private int id;
	private EventType type;
	private HashMap<Integer, Event> events= new HashMap<>();
	
	public ReservationEvent(int id, EventType type) {
		this.id = id;
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public EventType getType() {
		return type;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public List<Integer> getListEvents() {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}

	public int addEvent(LocalDate localDate, String name, Place place, Duration duration) {
		Event e = new Event(++cpt, localDate, name, place, duration);
		events.put(e.getId(), e);
		return e.getId();
	}

	public boolean removeEvent(Event event) {
		events.remove(event.getId(), event);
		return true;
	}

	public boolean takeReservation(int eid, int placesToTake) throws EventNotFoundException {
		Event event = getEvent(eid);
		try {
			event.takeReservation(placesToTake);
		} catch (EventFullException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	private Event getEvent(int eid) throws EventNotFoundException {
		for (Entry<Integer, Event> event : events.entrySet()) {
			if(event.getKey() == eid)
				return event.getValue();
			
		}
		throw new EventNotFoundException("No event with the id "+ eid + " found");
	}

	public String getEventAsString(int eid) {
		String eventStr= type.getName();
		try {
			Event e = getEvent(eid);
			eventStr += " " + e.toString();
		} catch (EventNotFoundException e) {
			eventStr += " " + e.getMessage();
		}
		return eventStr;
	}
	
}
