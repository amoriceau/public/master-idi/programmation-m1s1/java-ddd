package domain.aggregates;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import domain.entities.Event;
import domain.vo.Duration;
import domain.vo.EventType;
import domain.vo.Place;
import exceptions.EventFullException;
import exceptions.EventNotFoundException;

public class ReservationEvent {
	private static int cpt = 0;
	private int id;
	private EventType type;
	private HashMap<Integer, Event> events= new HashMap<>();
	
	public ReservationEvent(int id, EventType type) {
		this.id = id;
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public EventType getType() {
		return type;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public List<Integer> getListEvents() {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}
	
	public List<Integer> getListEvents(LocalDate date) {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			if(entry.getValue().getDate().equals(date))
				tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}
	
	public List<Integer> getListEventsFrom(LocalDate date) {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			if(entry.getValue().getDate().isAfter(date))
				tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}
	
	public List<Integer> getListEventsBefore(LocalDate date) {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			if(entry.getValue().getDate().isBefore(date))
				tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}
	
	public List<Integer> getListEvents(LocalDate from, LocalDate to) {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			if(entry.getValue().getDate().isAfter(from) || entry.getValue().getDate().isBefore(to))
				tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}
	
	public List<Integer> getListEvents(Place place) {
		List<Integer> tmpEvents = new LinkedList<>();
		for (Entry<Integer, Event> entry : events.entrySet()) {
			if(entry.getValue().getPlace().equals(place))
				tmpEvents.add(entry.getKey());
			
		}
		return tmpEvents;
	}

	public int addEvent(LocalDate localDate, String name, Place place, Duration duration, float price) {
		Event e = new Event(++cpt, localDate, name, place, duration, price);
		events.put(e.getId(), e);
		return e.getId();
	}

	public boolean removeEvent(Event event) {
		events.remove(event.getId(), event);
		return true;
	}

	public boolean takeReservation(int eid, int placesToTake) throws EventNotFoundException, EventFullException {
		Event event = getEvent(eid);
		try {
			event.takeReservation(placesToTake);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	public boolean freeReservation(int eid, int placesToFree) throws Exception {
		Event event = getEvent(eid);
		try {
			event.freeReservation(placesToFree);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	private Event getEvent(int eid) throws EventNotFoundException {
		for (Entry<Integer, Event> event : events.entrySet()) {
			if(event.getKey() == eid)
				return event.getValue();
			
		}
		throw new EventNotFoundException("No event with the id "+ eid + " found");
	}

	public String getEventAsString(int eid) {
		String eventStr= type.getName();
		try {
			Event e = getEvent(eid);
			eventStr += " " + e.toString();
		} catch (EventNotFoundException e) {
			eventStr += " " + e.getMessage();
		}
		return eventStr;
	}

	public boolean isEventFull(int eid) throws EventNotFoundException {
		return getEvent(eid).isFull();
	}
	
	public int getNumberOfVisitorsInPlace(Place place) throws EventNotFoundException {
		int total = 0;
		for (Integer eid : this.getListEvents(place)) {
			total+= getEvent(eid).getReservations();
		}
		
		return total;
	}
	
}
