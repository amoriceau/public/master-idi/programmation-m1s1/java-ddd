package application;

import java.time.LocalDate;
import java.util.List;

import domain.aggregates.ReservationEvent;
import domain.entities.Event;
import domain.repository.Repository;
import domain.vo.Duration;
import domain.vo.EventType;
import domain.vo.Place;
import exceptions.EventNotFoundException;
import infrastructure.ReservationRepository;

public class BilleterieApplication {

	private final Repository repo = new ReservationRepository();
	private static int cpt = 0;
	
	public int createAggregate(EventType type) {
		ReservationEvent r = new ReservationEvent(++cpt, type);
		repo.save(r);
		return cpt;
	}
	
	public List<Integer> getListEvents(int aggid){
		ReservationEvent r = repo.findById(aggid);
		return r.getListEvents();
	}
	
	public String getEventAsString(int aggid, int eid){
		ReservationEvent r = repo.findById(aggid);
		return r.getEventAsString(eid);
	}
	
	public int addEvent(int aggid, LocalDate localDate, String name, Place place, Duration duration){
		ReservationEvent r = repo.findById(aggid);
		return r.addEvent(localDate, name, place, duration);
	}
	
	public boolean removeEvent(int aggid, Event event){
		ReservationEvent r = repo.findById(aggid);
		return r.removeEvent(event);
	}
	
	public boolean takeReservation(int aggid, int eid, int placesToTake) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.takeReservation(eid, placesToTake);
	}

	public boolean isEventFull(int aggid, int eid) throws EventNotFoundException {
		ReservationEvent r = repo.findById(aggid);
		return r.isEventFull(eid);
	}

}
