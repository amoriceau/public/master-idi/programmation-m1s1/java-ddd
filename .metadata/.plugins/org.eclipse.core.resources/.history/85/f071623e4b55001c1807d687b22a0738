package domain.entities;

import java.util.Date;

import domain.vo.Duration;
import domain.vo.Place;
import exceptions.EventFullException;

public class Event {

	private int id;
	private int reservations = 0;
	private Date date;
	private String name;
	private Duration duration;
	private Place place;
	
	/**
	 * @param reservations
	 * @param date
	 * @param name
	 * @param duration
	 */
	public Event(int id, Date date, String name, Place place, Duration duration) {
		this.id = id;
		this.date = date;
		this.name = name;
		this.duration = duration;
		this.place = place;
	}
	
	
	/**
	 * @return the reservations
	 */
	public int getReservations() {
		return reservations;
	}
	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}
	
	/**
	 * @return the place
	 */
	public Place getPlace() {
		return place;
	}


	public Integer getId() {
		return id;
	}
	
	public void takeReservation(int placesToTake) throws EventFullException {
		if(reservations + placesToTake > place.getCapacity()) {
			int placesLeft = place.getCapacity()-reservations;
			throw new EventFullException("Event has only "+ placesLeft +" places left.");
		}
		reservations += placesToTake;
	}
	
	public void freeReservation(int placesToFree) {
		reservations = Math.max(reservations - placesToFree, 0);
	}
	
	
}
