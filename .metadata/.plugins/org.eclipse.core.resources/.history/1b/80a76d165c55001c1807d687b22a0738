package main;

import java.time.LocalDate;

import application.BilleterieApplication;
import domain.vo.Address;
import domain.vo.City;
import domain.vo.Duration;
import domain.vo.EventType;
import domain.vo.Place;
import exceptions.EventFullException;
import exceptions.EventNotFoundException;

public class Main {

	private final static BilleterieApplication application = new BilleterieApplication();
	
	public static void main(String[] args) throws Exception {
		// Create application

		// Create Value Objects
		City bordeaux = new City("Bordeaux", 33000);
		City paris = new City("Paris", 33000);

		Address a1 = new Address("Victor Hugo", 25, bordeaux);
		Address a2 = new Address("Henry Delaunay", 1, paris);
		Address a3 = new Address("Somewhere over the rainbow", 1, paris);

		Place femina = new Place("Femina", a1, 800);
		Place sdf = new Place("Stade de France", a2, 81000);
		Place miniSalle = new Place("Mini salle", a2, 3);

		EventType concert = new EventType("concert");
		EventType spectacle = new EventType("spectacle");
		EventType match = new EventType("match");

		int concertAgg = application.createAggregate(concert);
		int spectacleAgg = application.createAggregate(spectacle);
		int matchAgg = application.createAggregate(match);

		int c1 = application.addEvent(concertAgg, LocalDate.of(2021, 12, 24), "Concert de Noel - NRJ", sdf,
				new Duration(3, 30), 35.0f);
		int c2 = application.addEvent(concertAgg, LocalDate.of(2022, 7, 28), "Ed Sheeran", sdf, new Duration(2, 0), 69.99f);
		
		int c3 = application.addEvent(concertAgg, LocalDate.of(2022, 7, 28), "Le concert des enfoirés", femina, new Duration(6, 8, 43), 12.5f);

		int s1 = application.addEvent(spectacleAgg, LocalDate.of(2022, 1, 16), "Gad El Maleh", femina,
				new Duration(1, 45), 18.5f);
		int s2 = application.addEvent(spectacleAgg, LocalDate.of(2022, 7, 28), "Franjo", femina, new Duration(2, 0), 10.0f);
		
		int s3 = application.addEvent(spectacleAgg, LocalDate.of(2022, 7, 28), "Evenement mystere", miniSalle, new Duration(79, 8, 33), 75.0f);

		int m1 = application.addEvent(matchAgg, LocalDate.of(2022, 4, 12), "France - Italie", sdf, new Duration(2, 0), 89.0f);
		
		
		
		System.out.println(application.getEventAsString(concertAgg, c1));
		System.out.println("-".repeat(50));
		System.out.println(application.getEventAsString(concertAgg, s1));
		System.out.println("-".repeat(50));
		System.out.println(application.getEventAsString(spectacleAgg, s1));
		System.out.println("-".repeat(50));
		System.out.println(application.getEventAsString(spectacleAgg, s3));
		System.out.println("-".repeat(50));
		
		application.takeReservation(spectacleAgg, s3, 2);

		System.out.println(application.getEventAsString(spectacleAgg, s3));
		System.out.println("-".repeat(50));
		application.takeReservation(spectacleAgg, s3, 2);
		application.takeReservation(spectacleAgg, s3, 1);
		System.out.println(application.getEventAsString(spectacleAgg, s3));
		System.out.println("-".repeat(50));
		System.out.println(application.isEventFull(spectacleAgg, s3));
		System.out.println("-".repeat(50));
		
		
		
		System.out.println(application.getEventAsString(concertAgg, c1));
		System.out.println("-".repeat(50));
		application.takeReservation(concertAgg, c1, 20);
		application.takeReservation(concertAgg, c1, -1);
		
		application.takeReservation(concertAgg, c3, 20);
		application.takeReservation(concertAgg, c2, 56000);
		System.out.println(application.getEventAsString(concertAgg, c1));
		System.out.println("-".repeat(50));
		application.freeReservation(concertAgg, c1, 5);
		application.freeReservation(concertAgg, c1, -1);
		System.out.println(application.getEventAsString(concertAgg, c1));
		
		
		System.out.println("-".repeat(50));
		Integer[] agg = {spectacleAgg, matchAgg, concertAgg};
		createReservations(agg);
		
		System.out.println("-".repeat(50));
		for (Integer id : application.getListEvents(concertAgg, femina)) {
			System.out.println(application.getEventAsString(concertAgg, id));
		}
		System.out.println("-".repeat(50));
		
		for (Integer id : application.getListEventsBefore(concertAgg, LocalDate.of(2022, 7, 29))) {
			System.out.println(application.getEventAsString(concertAgg, id));
		}
		System.out.println("-".repeat(50));
		
		System.out.println(application.getNumberOfVisitorsInPlace(spectacleAgg, sdf));
	}

	private static void createReservations(Integer[] aggregates, boolean print) {
		for (Integer aggregate : aggregates) {
			for (Integer eid : application.getListEvents(aggregate)) {
				try {
					application.takeReservation(aggregate, eid, (int)Math.random()*application.getEventMaxCapacity(aggregate, eid));
					if(print) {
						System.out.println(application.getEventAsString(aggregate, eid));
					}
				} catch (EventNotFoundException | EventFullException e) {
					
					System.out.println(e.getMessage());
				}
			}
		}
		
	}
}
