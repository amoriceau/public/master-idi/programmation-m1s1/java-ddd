package domain.entities;

import java.time.LocalDate;
import java.util.Date;

import domain.vo.Duration;
import domain.vo.Place;
import exceptions.EventFullException;

public class Event {

	private int id;
	private int reservations = 0;
	private LocalDate date;
	private String name;
	private Duration duration;
	private Place place;
	
	/**
	 * @param reservations
	 * @param localDate
	 * @param name
	 * @param duration
	 */
	public Event(int id, LocalDate localDate, String name, Place place, Duration duration) {
		this.id = id;
		this.date = localDate;
		this.name = name;
		this.duration = duration;
		this.place = place;
	}
	
	
	/**
	 * @return the reservations
	 */
	public int getReservations() {
		return reservations;
	}
	
	/**
	 * @return the date
	 */
	public LocalDate getDate() {
		return date;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}
	
	/**
	 * @return the place
	 */
	public Place getPlace() {
		return place;
	}


	public Integer getId() {
		return id;
	}
	
	public void takeReservation(int placesToTake) throws EventFullException {
		if(reservations + placesToTake > place.getCapacity()) {
			int placesLeft = place.getCapacity()-reservations;
			throw new EventFullException("Event has only "+ placesLeft +" places left.");
		}
		reservations += placesToTake;
	}
	
	public void freeReservation(int placesToFree) {
		reservations = Math.max(reservations - placesToFree, 0);
	}


	@Override
	public String toString() {
		return "Event "+name+"\n reservations=" + reservations + ", date=" + date
				+ ", duration=" + duration + ", place=" + place + "]";
	}
	
	
}
